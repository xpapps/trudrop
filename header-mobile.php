<!DOCTYPE html>



<html <?php language_attributes(); ?>>



<head>

    <meta charset="UTF-8">

    <title><?php wp_title(); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--FAVICON-->

    <link rel="icon" href="<?php echo theme_get_option( 'favicon_image' ); ?>" type="image/x-icon">

    <!-- ESTILOS-->

    <link href="<?php bloginfo('stylesheet_directory'); ?>/dist/lib.css" rel="stylesheet">

    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/styles-trudrop.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- JS -->

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,300italic,300,200italic,200,600,700,600italic,700italic,900,900italic' rel='stylesheet' type='text/css'>


</head>
