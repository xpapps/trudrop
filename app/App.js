// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
//
// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Open up App.js to start working on your app!</Text>
//       </View>
//     );
//   }
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView, Dimensions} from 'react-native';
import Drawer from 'react-native-drawer';

import {AppLoading, Font, Asset} from 'expo';

import PageView from './components/PageView';
import Header from './components/Header';

export default class App extends React.Component {

    constructor() {
        super();

        this.state = {
            pageName: 'Home',
            pageKey: 0,
            isLoadingComplete: false,
        }
    }

    componentDidMount() {
        this.setState({
            pageUrl: 'http://trudrop.com/Home/',
            pageName: 'Home',
        });
    }

    getWVRef = (webViewRef) => {
        this.setState({webViewRef});
    }

    render() {
        let {width, height} = Dimensions.get('window');
        if (!this.state.isLoadingComplete) {
            return (
                <AppLoading
                    startAsync={this._loadResourcesAsync}
                    onFinish={this._handleFinishLoading}
                />
            );
        } else {
            return (
                <Drawer
                    type={"overlay"}
                    ref={(ref) => this._drawer = ref}
                    tapToClose={true}
                    openDrawerOffset={0.5}
                    panCloseMask={0.5}
                    closedDrawerOffset={-3}
                    tweenHandler={(ratio) => ({
                        main: {opacity: (2 - ratio) / 2}
                    })}
                    content={<ControlPanel activePage={this.state.pageName} changePageUrl={this.changePageUrl}
                                           openDrawer={this.openDrawer}/>}>

                    <Header name={this.state.pageName} openMenu={this.openDrawer}>
                    </Header>
                    <ScrollView contentContainerStyle={[styles.container, {height: height*1.3, width}]}>
                        <PageView getRef={this.getWVRef} key={this.state.pageKey} url={this.state.pageUrl}/>
                    </ScrollView>
                </Drawer>
            );
        }
    }

    _handleFinishLoading = () => {
        this.setState({isLoadingComplete: true});
    };

    _loadResourcesAsync = async () => {
        await Font.loadAsync({
            'Roboto': require("native-base/Fonts/Roboto.ttf"),
            'Roboto_medium': require("native-base/Fonts/Roboto_medium.ttf")
        });
    }

    changePageUrl = async (item) => {
        let {pageKey} = this.state;

        this.setState({pageName: item.name, pageUrl: item.url})
        await this._drawer.close()
        if (this.state.webViewRef)
            this.state.webViewRef.reload();
    }

    openDrawer = () => {
        this._drawer.open();
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 0
    },
});

const Item = ({name, url, renderPage, isActive}) => (
    <View style={[{padding: 15}, isActive ? {backgroundColor: 'lightgray', borderRadius: 5} : {}]}>
        <Text onPress={() => renderPage({url, name})}>{name}</Text>
    </View>
)


const Pages = [
    {url: 'http://trudrop.com/Home/', name: 'Home'},
    {url: 'http://trudrop.com/Video/', name: 'Video'},
    {url: 'http://trudrop.com/About/', name: 'About'},
    {url: 'http://trudrop.com/Quick-Start/', name: 'Quick Start'},
    {url: 'http://trudrop.com/benefits/', name: 'Benefits'},
    {url: 'http://trudrop.com/Durability/', name: 'Durability'},
    {url: 'http://trudrop.com/Recyclable/', name: 'Recyclable'},
    {url: 'http://trudrop.com/Testimonials/', name: 'Testimonials'},
    {url: 'http://trudrop.com/contact-us/', name: 'Contact Us'}
]

const ControlPanel = ({openDrawer, changePageUrl, activePage}) => (
    <View style={{alignItems: 'center', backgroundColor: 'white', paddingTop: 100, height: '100%'}}>
        {
            Pages.map((item, key) => (
                <Item isActive={activePage === item.name} key={key} url={item.url} name={item.name}
                      renderPage={changePageUrl}/>
            ))
        }
    </View>
)