import React from 'react';
import {StyleSheet, WebView, Dimensions} from 'react-native';

export default class PageView extends React.Component {

    constructor() {
        super();
        this.state = {height: Dimensions.get('window').height}
    }

    componentDidMount() {
    }

    sendRefToParent = (ref) => {
        // this.props.getRef(ref);
    }

    render() {
        let {width, height} = Dimensions.get('window');
        return (
            <WebView
                scrollEnabled={true}
                startInLoadingState={true}
                javaScriptEnabled={true}
                bounces={false}
                automaticallyAdjustContentInsets={false}
                scalesPageToFit={false}
                style={{position: 'relative', top: -5, height: height*1.3, width, flex: 1, paddingBottom: 0}}
                source={{uri: this.props.url}}
            />
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});