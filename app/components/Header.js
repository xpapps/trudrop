import React, {Component} from 'react';
import {Text, View, Container, Header, Left, Body, Right, Button, Icon, Title} from 'native-base';

import {StatusBar, Image} from 'react-native';

export default class HeaderIconExample extends Component {

    static defaultProps = {
        name: 'Home'
    }

    componentDidMount() {
        this.setState({isReady: true});
    }

    constructor() {
        super();
        this.state = {
            isReady: false
        }
    }

    render() {
        return (
            <View style={{paddingTop: StatusBar.currentHeight}}>
                <Header style={{backgroundColor: 'white'}}>
                    <Left>
                        <Button transparent onPress={this.props.openMenu} style={{color: 'black'}}>
                            <Icon name='menu' style={{color: 'black'}} color={'black'}/>
                        </Button>
                    </Left>
                    <Body>
                    {
                        this.props.name && this.props.name === 'Home' ?
                            <Image
                                source={require('../assets/logo.jpg')}
                                style={{resizeMode: 'contain', width: 180, height: 44}}>
                            </Image> : <Text style={{color: 'black', fontWeight: 'bold'}}>{this.props.name}</Text>
                    }
                    </Body>
                    <Right>
                    </Right>
                </Header>
            </View>
        )
    }
}