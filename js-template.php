<?php
/**
 * Template Name: Clean Page - js
 * This template will only display the content you entered in the page editor
 */
?>
 
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php get_header('mobile');?>
	
<?php if(is_page( 121 )  ) : ?>
	<?php putRevSlider("trudrop-desktop") ?>
<?php endif; ?>

<?php if(is_page( 119 )  ) : ?>
	<?php putRevSlider("quick-start") ?>
<?php endif; ?>

<?php if(is_page( 127 )  ) : ?>
    <?php putRevSlider("testimonials") ?>
<?php endif; ?>


    <?php wp_head(); ?>
</head>
<body>





<?php wp_footer(); ?>
</body>
</html>


